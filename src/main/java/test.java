import org.springframework.boot.SpringApplication;

/************************************************************************************************
 * Created by ZIMAI on 2016/12/15.
 * 这只是Java规范的一个标准的应用程序入口方法。
 * 我们的main方法委托了SpringApplication类去调用run方法完成这个功能。S
 * pringApplication会启动我们的应用程序，启动Spring去开启自配置的Tomcat Web服务器。
 * 我们需要把Example.class作为参数传送到run方法，告知SpringApplication这是一个主要是的Spring组件。
 * args字符串数组也传入了，以处理命令行提供的参数。
 ***********************************************************************************************/
public class test {

    public static void main(String[] args) {
        SpringApplication.run(example.class,args);
    }

}
