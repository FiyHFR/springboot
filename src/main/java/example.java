import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ZIMAI on 2016/12/15.
 */
@RestController   //这是一个spring构造注解，现在这个类是一个web控制器，spring会使用它处理web请求；返回字符串
@EnableAutoConfiguration //这个注解告诉spring-boot，你是如何配置spring的，这基于你的依赖包。
public class example {
    @RequestMapping("/") //这个注解展示了路由信息，告诉spring，所有路径“/”的http请求，都交给home方法处理；
    public String home(){
        return "Hello World!";
    }

}
